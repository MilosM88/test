package primeri22;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Primer8 {
	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("http://www.calculator.net");
		java.util.List<WebElement> links = driver.findElements(By.tagName("a"));
		System.out.println("Number of Links in the Page is " + links.size());
		//for (int i = 0; i < links.size(); ++i) {
			//System.out.println("Name of Link# " + i + "-" + links.get(i).getText());
		//}
		WebElement time = driver.findElement(By.linkText("Time"));
		System.out.println("Select by Link Text: " + time.getText());
		
		WebElement volume = driver.findElement(By.partialLinkText("Volume"));
		System.out.println("Select by Link Partial Text: " + volume.getText());
		
		WebElement search = driver.findElement(By.name("sa"));
		System.out.println("Selected by name: " + search.getAttribute("value"));
		System.out.println("Selected by name: " + search.getAttribute("name"));
		driver.close();
	}
}
