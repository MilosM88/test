package primeri24;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PrimerValidacijaFormi {
	public static String get201Characters(){
		StringBuffer outputBuffer = new StringBuffer(201);
		for (int i = 0; i < 201; i++){
		   outputBuffer.append("a");
		}
		return outputBuffer.toString();
	}
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "/home/mkondic/data/ssluzba/materijali/5_kondic/22_termin/ssluzba/chromedriver");
		WebDriver driver = new ChromeDriver();
		FormPage formPage = new FormPage(driver);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.get("http://formvalidation.io/examples/complex-form/");
		WebDriver frame = (new WebDriverWait(driver,10))
				.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("demo-frame")));
		WebElement form = (new WebDriverWait(frame,10))
				.until(ExpectedConditions.presenceOfElementLocated(
				By.id("movieForm")));
		System.out.println(form.isDisplayed());
		//TODO 1 validate form elements visibility
		WebElement validateBtn = formPage.getValidateBtn();
		validateBtn.click();
		System.out.println(formPage.getTitleRequiredError().getText());
		System.out.println(formPage.getTitleLengthError().isDisplayed());
		System.out.println(validateBtn.isEnabled());
		
		formPage.setMovieTitle(get201Characters());
		
		System.out.println(formPage.getTitleRequiredError().isDisplayed());
		System.out.println(formPage.getTitleLengthError().getText());
		System.out.println(validateBtn.isEnabled());
		
		//TODO 2 validate all fields error messages
		driver.close();
	}
}
