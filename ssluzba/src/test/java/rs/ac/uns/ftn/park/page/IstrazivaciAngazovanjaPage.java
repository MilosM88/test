package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IstrazivaciAngazovanjaPage {

WebDriver driver;
	
	public IstrazivaciAngazovanjaPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getAngazujOsobu() {
		return driver.findElement(By.cssSelector("button[ng-click='addctrl.addConnection()']"));
	}
	
	public WebElement getZvanje() {
		return driver.findElement(By.cssSelector("[ng-model='connection.position']"));
	}
	
	public WebElement setZvanje(String value) {
		return driver.findElement(By.cssSelector("option[label='" + value + "']"));
	}
	
	public WebElement getFunkcija() {
		return driver.findElement(By.cssSelector("[ng-model='connection.function']"));
	}
	
	public WebElement setFunkcija(String value) {
		return driver.findElement(By.cssSelector("option[label='" + value + "']"));
	}
	
	public WebElement getProcenatZaposlenosti() {
		return driver.findElement(By.name("employmentPercentage"));
	}
	
	public void setProcenatZaposlenosti(String value) {
		WebElement input = getProcenatZaposlenosti();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getOd() {
		return driver.findElement(By.name("startDate"));
	}
	
	public void setOd(String value) {
		WebElement input = getOd();
		input.sendKeys("value");
	}
	
}
