package rs.ac.uns.ftn.park;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import rs.ac.uns.ftn.park.page.LoginTestPage;
import rs.ac.uns.ftn.park.page.MainPage;
import rs.ac.uns.ftn.park.page.OsnovniPodaciPage;

public class DodavanjeOsnovnihPodatakaInstitucije {

	private WebDriver browser;
	private LoginTestPage loginTestPage;
	private MainPage mainPage;
	private Actions action;
	private OsnovniPodaciPage osnovniPodaciPage;
	private WebDriverWait wait;
	
	@BeforeTest
	public void setup() {
		
		//instanciranje browsera
		browser = new FirefoxDriver();
		//implicitno vreme cekanja za pronalazenje elemenata
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//maximizuje prozor
		browser.manage().window().maximize();
		//navigacija na zeljenu stranicu
		browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
		//instanciranje loginTestPage-a
		loginTestPage = new LoginTestPage(browser);
		mainPage = new MainPage(browser);
		action = new Actions(browser);
		osnovniPodaciPage = new OsnovniPodaciPage(browser);
		wait = new WebDriverWait(browser, 10);
	}
	
	@Test(priority=1)
	public void uspesanLogin() throws InterruptedException {
			
		
		
		loginTestPage.getUsername().isDisplayed();
		loginTestPage.getPassword().isDisplayed();
		loginTestPage.btnPrijaviSe().isDisplayed();
		
		//set username value
		loginTestPage.setUsername("miloseko@gmail.com"); //send new value
		
		//set password value
		loginTestPage.setPassword("wqt5BRBC4pXfMYQrTWge");
		
		//click signin button
		loginTestPage.btnPrijaviSe().click();

		assertEquals(mainPage.getLogo().isDisplayed(), true);
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/admin-institution/", browser.getCurrentUrl());
		
	}
	
	@Test(priority=2)
	public void uspesnoDodavanjeInstitucijeOsnovniPodaci() {
		
		assertEquals(mainPage.getLogo().isDisplayed(), true);
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/admin-institution/", browser.getCurrentUrl());
		
		osnovniPodaciPage.getName().isDisplayed();
		osnovniPodaciPage.getEngName().isDisplayed();
		osnovniPodaciPage.getState().isDisplayed();
		osnovniPodaciPage.getPlace().isDisplayed();
		osnovniPodaciPage.getOpstina().isDisplayed();
		osnovniPodaciPage.getAddress().isDisplayed();
		osnovniPodaciPage.getWebAddress().isDisplayed();
		osnovniPodaciPage.getEmail().isDisplayed();
		osnovniPodaciPage.getPhone().isDisplayed();
		osnovniPodaciPage.getAcronym().isDisplayed();
		
		osnovniPodaciPage.setName("Institucija Vezba");
		osnovniPodaciPage.setEngName("Institution Practice");
		osnovniPodaciPage.getStateIzaberi("Srbija");
		osnovniPodaciPage.setPlace("Beograd");
		osnovniPodaciPage.setOpstina("Beograd");
		osnovniPodaciPage.setAddress("Prva ulica 10");
		osnovniPodaciPage.setWebAddress("vezba.com");
		osnovniPodaciPage.setEmail("vezba@mail.com");
		osnovniPodaciPage.setPhone("987654321");
		osnovniPodaciPage.setAcronym("VI");
		
		osnovniPodaciPage.getSacuvajBtn().click();
		assertEquals(mainPage.getSuccess().isDisplayed(), true);
		action.moveToElement(mainPage.getSuccess()).perform();
		mainPage.getCloserBtn().click();
		
	}
	
	@Test(priority=3)
	public void uspesanLogout() {
		
		assertEquals(mainPage.getPadajuciMeni().isDisplayed(), true);
		mainPage.getPadajuciMeni().click();
		assertEquals(mainPage.getOdjaviSe().isDisplayed(), true);
		mainPage.getOdjaviSe().click();
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/login", browser.getCurrentUrl());
	}
	
}
