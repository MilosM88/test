package rs.ac.uns.ftn.park;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import rs.ac.uns.ftn.park.page.IstrazivaciLicniPodaciPage;
import rs.ac.uns.ftn.park.page.IstrazivaciStranaPage;
import rs.ac.uns.ftn.park.page.LoginTestPage;
import rs.ac.uns.ftn.park.page.MainPage;

public class Vezba {
	private WebDriver browser;
	private LoginTestPage loginTestPage;
	private MainPage mainPage;
	private IstrazivaciStranaPage istrazivaciStranaPage;
	private IstrazivaciLicniPodaciPage istrazivaciLicniPodaciPage;
	private Actions action;
	
	@BeforeMethod
	public void setup() {
		
		//instanciranje browsera
		browser = new FirefoxDriver();
		//implicitno vreme cekanja za pronalazenje elemenata
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//maximizuje prozor
		browser.manage().window().maximize();
		//navigacija na zeljenu stranicu
		browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
		//instanciranje loginTestPage-a
		loginTestPage = new LoginTestPage(browser);
		mainPage = new MainPage(browser);
		istrazivaciStranaPage = new IstrazivaciStranaPage(browser);
		istrazivaciLicniPodaciPage = new IstrazivaciLicniPodaciPage(browser);
		action = new Actions(browser);
	}
	
	@Test
	public void uspesanLogin() throws InterruptedException {
		
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/login", browser.getCurrentUrl());
			
		loginTestPage.getUsername().isDisplayed();
		loginTestPage.getPassword().isDisplayed();
		loginTestPage.btnPrijaviSe().isDisplayed();
		
		//set username value
		loginTestPage.setUsername("miloseko@gmail.com"); //send new value
		
		//set password value
		loginTestPage.setPassword("wqt5BRBC4pXfMYQrTWge");
		
		//click signin button
		loginTestPage.btnPrijaviSe().click();
		
		
		WebElement logo = browser.findElement(By.className("logo-lg"));	
		assertEquals(logo.isDisplayed(), true);
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/admin-institution/", browser.getCurrentUrl());
		mainPage.getIstrazivaci().isDisplayed();
		mainPage.getIstrazivaci().click();
		
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/persons", browser.getCurrentUrl());
		istrazivaciStranaPage.getDodajIstrazivacaBtn().isDisplayed();
		istrazivaciStranaPage.getDodajIstrazivacaBtn().click();
		
		istrazivaciLicniPodaciPage.getIme().isDisplayed();
		istrazivaciLicniPodaciPage.setIme("Zvonko");
		istrazivaciLicniPodaciPage.setPrezime("Bogdan");
		istrazivaciLicniPodaciPage.setImeJednogRoditelja("Zvonkovi");
		//istrazivaciLicniPodaciPage.getTitulaIstrazivaca().click();
		//moze biti "dr", "mr" ili "none"
		istrazivaciLicniPodaciPage.getTitulaIstrazivacaIzaberi("dr").click();
		istrazivaciLicniPodaciPage.setRodjendan("11.11.1911");
		istrazivaciLicniPodaciPage.setDrzavaRodjenja("Srbija");
		istrazivaciLicniPodaciPage.setMestoRodjenja("Sombor");
		istrazivaciLicniPodaciPage.setOpstinaRodjenja("Sombor");
		istrazivaciLicniPodaciPage.setDrzavaBoravista("Srbija");
		istrazivaciLicniPodaciPage.setMestoBoravista("Sombor");
		istrazivaciLicniPodaciPage.setOpstinaBoravista("Sombor");
		istrazivaciLicniPodaciPage.setAdresa("Apatinski put 10");
		//Pol moze biti "Muški" ili "Ženski"
 		istrazivaciLicniPodaciPage.getPol("Muški").click();
 		istrazivaciLicniPodaciPage.setJmbgBrojPasosa("123456789");
 		istrazivaciLicniPodaciPage.setEmail("zvonkoBogdan@mail.com");
 		istrazivaciLicniPodaciPage.setTelefon("1122332211");
 		istrazivaciLicniPodaciPage.setLicnaVebAdresa("zvonko.com");
 		//Status moze biti "Status 1", "Status 2" ili "Status 3"
 		istrazivaciLicniPodaciPage.getStatusIstrazivacaIzaberi("Status 2").click();
 		
 		istrazivaciLicniPodaciPage.getSacuvaj().click();
 		assertEquals(mainPage.getSuccess().isDisplayed(), true);
 		action.moveToElement(mainPage.getSuccess()).perform();
 		mainPage.getCloserBtn().click();
	}
	
	@AfterMethod
	public void closeSelenium() {
		// Shutdown the browser
	//	browser.quit();
	}
}
