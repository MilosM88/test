package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class MainPage {

	WebDriver driver;
	
	public MainPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getLogo() {
		return driver.findElement(By.className("logo-lg"));
	}
	
	public WebElement getInstitucija() {
		return driver.findElement(By.className("fa-university"));
	}
	
	public WebElement getIstrazivaci() {
		return driver.findElement(By.className("fa-user"));
	}
	
	public WebElement getSideBarToggle() {
		return driver.findElement(By.className("sidebar-toggle"));
	}
	
	public WebElement getPadajuciMeni() {
		return driver.findElement(By.cssSelector("a[class='dropdown-toggle']"));
	}
	
	public WebElement getUputsvo() {
		return driver.findElement(By.cssSelector("a[href='app/assets/docs/Uputstvo.pdf']"));
	}
	
	public WebElement getKontakt() {
		return driver.findElement(By.cssSelector("a[href='mailto:minis@mpn.gov.rs']"));
	}
	
	public WebElement getPromeniSifru() {
		return driver.findElement(By.cssSelector("a[ui-sref='changePassword']"));
	}
	
	public WebElement getOdjaviSe() {
		return driver.findElement(By.cssSelector("a[ng-click='logOut()']"));
	}
	
	public WebElement getSuccess() {
		return driver.findElement(By.cssSelector("h4[class='ui-pnotify-title']"));
	}
	
	public WebElement getCloserBtn() {
		return driver.findElement(By.cssSelector("[class='ui-pnotify-closer']"));
	}
	
}
