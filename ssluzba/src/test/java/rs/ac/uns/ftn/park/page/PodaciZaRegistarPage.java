package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PodaciZaRegistarPage {

	WebDriver driver;
	
	public PodaciZaRegistarPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getIstrazivaciStrana() {
		return driver.findElement(By.className("fa-user"));
	}
	
	public WebElement getOsnovniPodaci() {
		return driver.findElement(By.linkText("Osnovni podaci"));
	}
	
	public WebElement getPodaciZaProjekte() {
		return driver.findElement(By.linkText("Podaci za projekte"));
	}
	
	public WebElement getIstrazivaci() {
		return driver.findElement(By.linkText("Istraživači"));
	}
	
	public WebElement getPib() {
		return driver.findElement(By.name("pib"));
	}
	
	public void setPib(String value) {
		WebElement input = getPib();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getMaticniBroj() {
		return driver.findElement(By.name("maticniBroj"));
	}
	
	public void setMaticniBroj(String value) {
		WebElement input = getMaticniBroj();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getBrojPoslednjeNaucneAkreditacije() {
		return driver.findElement(By.name("accreditationNumber"));
	}
	
	public void setBrojPoslednjeNaucneAkreditacije(String value) {
		WebElement input = getBrojPoslednjeNaucneAkreditacije();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getDatumPoslednjeNaucneAkreditacije() {
		return driver.findElement(By.name("accreditationDate"));
	}
	
	public void setDatumPoslednjeNaucneAkreditacije(String value) {
		WebElement input = getDatumPoslednjeNaucneAkreditacije();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getNazivInstitucijeIzAkreditacije() {
		return driver.findElement(By.name("accreditationNote"));
	}
	
	public void setNazivInstitucijeIzAkreditacije(String value) {
		WebElement input = getNazivInstitucijeIzAkreditacije();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getNapomenaORegistru() {
		return driver.findElement(By.name("note"));
	}

	public void setNapomenaORegistru(String value) {
		WebElement input = getNapomenaORegistru();
		input.clear();
		input.sendKeys(value);
	}
	
	//OVDE DOVRSITI VRSTU INSTITUCIJE
	
	//OVDE DOVRSITI OSNOVNU DELATNOST INSTITUCIJE
	
	public WebElement getOsnivac() {
		return driver.findElement(By.name("founder"));
	}

	public void setOsnivac(String value) {
		WebElement input = getOsnivac();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getDatumOsnivanja() {
		return driver.findElement(By.name("date"));
	}

	public void setDatumOsnivanja(String value) {
		WebElement input = getDatumOsnivanja();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getBrojResenjaOOsnivanju() {
		return driver.findElement(By.name("rescriptNumber"));
	}

	public void setBrojResenjaOOsnivanju(String value) {
		WebElement input = getBrojResenjaOOsnivanju();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getVlasnickaStruktura() {
		return driver.findElement(By.name("ownershipStructure"));
	}

	public void setVlasnickaStruktura(String value) {
		WebElement input = getVlasnickaStruktura();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getOdustaniBtn() {
		return driver.findElement(By.className("fa-times"));
	}
	
	public WebElement getSacuvajBtn() {
		return driver.findElement(By.className("fa-check"));
	}
	
}
