package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PodaciZaProjektePage {

	WebDriver driver;
	
	public PodaciZaProjektePage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getIstrazivaciStrana() {
		return driver.findElement(By.className("fa-user"));
	}
	
	public WebElement getOsnovniPodaci() {
		return driver.findElement(By.linkText("Osnovni podaci"));
	}
	
	public WebElement getPodaciZaRegistar() {
		return driver.findElement(By.linkText("Podaci za registar"));
	}
	
	public WebElement getIstrazivaci() {
		return driver.findElement(By.linkText("Istraživači"));
	}
	
	public WebElement getBrojRacuna() {
		return driver.findElement(By.name("account"));
	}
	
	public void setBrojRacuna(String value) {
		WebElement input = getBrojRacuna();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getIdBrojNaMedjNivou() {
		return driver.findElement(By.name("orcid"));
	}
	
	public void setIdBrojNaMedjNivou(String value) {
		WebElement input = getIdBrojNaMedjNivou();
		input.clear();
		input.sendKeys(value);
	}
	
	// OVDE DOVRSITI STATUS INSTITUCIJE
	
	public WebElement getOdustaniBtn() {
		return driver.findElement(By.className("fa-times"));
	}
	
	public WebElement getSacuvajBtn() {
		return driver.findElement(By.className("fa-check"));
	}
	
}
