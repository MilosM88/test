package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IstrazivaciPage {

	WebDriver driver;
	
	public IstrazivaciPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getIstrazivaciStrana() {
		return driver.findElement(By.className("fa-user"));
	}
	
	public WebElement getOsnovniPodaci() {
		return driver.findElement(By.linkText("Osnovni podaci"));
	}
	
	public WebElement getPodaciZaRegistar() {
		return driver.findElement(By.linkText("Podaci za registar"));
	}
	
	public WebElement getPodaciZaProjekte() {
		return driver.findElement(By.linkText("Podaci za projekte"));
	}
	
	public WebElement getAngazujOsobu() {
		return driver.findElement(By.cssSelector("[ng-click='addctrl.savePersonConnection(Persons)']"));
	}
	
	public WebElement getOsoba() {
		return driver.findElement(By.cssSelector("input[placeholder='Pretraži osobe']"));
	}
	
	public void setOsoba(String value) {
		WebElement input = getOsoba();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getZvanje(String value) {
		return driver.findElement(By.cssSelector("select[label='" + value + "']"));
	}
	
	// ODREDITI FUNKCIJA
	
	public WebElement getProcenatZaposlenosti() {
		return driver.findElement(By.name("employmentPercentage"));
	}
	
	public void setProcenatZaposlenosti(String value) {
		WebElement input = getProcenatZaposlenosti();
		input.clear();
		input.sendKeys(value);
	}
	
	// PROVERITI DA LI OVO VALJA   css=input[ng-model='connection.startDate']
	public WebElement getMaxBrojMeseciNaProjektima() {
		return driver.findElement(By.cssSelector("select[ng-model='connection.maxMonthEngagement']"));
	}
	
	public WebElement getOdustaniBtn() {
		return driver.findElement(By.cssSelector("button[ng-click='addctrl.cancel(Persons)']"));
	}
	
	public WebElement getAngazujBtn() {
		return driver.findElement(By.cssSelector("button[ng-click='addctrl.savePersonConnection(Persons)']"));
	}
}
