package rs.ac.uns.ftn.park;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import rs.ac.uns.ftn.park.page.IstrazivaciAngazovanjaPage;
import rs.ac.uns.ftn.park.page.IstrazivaciLicniPodaciPage;
import rs.ac.uns.ftn.park.page.IstrazivaciStranaPage;
import rs.ac.uns.ftn.park.page.LoginTestPage;
import rs.ac.uns.ftn.park.page.MainPage;

public class DodajAngazujIstrazivaca {

	private WebDriver browser;
	private LoginTestPage loginTestPage;
	private MainPage mainPage;
	private IstrazivaciStranaPage istrazivaciStranaPage;
	private IstrazivaciLicniPodaciPage istrazivaciLicniPodaciPage;
	private Actions action;
	private IstrazivaciAngazovanjaPage istrazivaciAngazovanjaPage;
	private WebDriverWait wait;
	
	@BeforeTest
	public void setup() {
		
		//instanciranje browsera
		browser = new FirefoxDriver();
		//implicitno vreme cekanja za pronalazenje elemenata
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//maximizuje prozor
		browser.manage().window().maximize();
		//navigacija na zeljenu stranicu
		browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
		//instanciranje loginTestPage-a
		loginTestPage = new LoginTestPage(browser);
		mainPage = new MainPage(browser);
		istrazivaciStranaPage = new IstrazivaciStranaPage(browser);
		istrazivaciLicniPodaciPage = new IstrazivaciLicniPodaciPage(browser);
		action = new Actions(browser);		
		istrazivaciAngazovanjaPage = new IstrazivaciAngazovanjaPage(browser);
		wait = new WebDriverWait(browser, 20);
	}
	
	@Test(priority=1)
	public void uspesanLogin() throws InterruptedException {
			
		
		
		loginTestPage.getUsername().isDisplayed();
		loginTestPage.getPassword().isDisplayed();
		loginTestPage.btnPrijaviSe().isDisplayed();
		
		//set username value
		loginTestPage.setUsername("miloseko@gmail.com"); //send new value
		
		//set password value
		loginTestPage.setPassword("wqt5BRBC4pXfMYQrTWge");
		
		//click signin button
		loginTestPage.btnPrijaviSe().click();

		assertEquals(mainPage.getLogo().isDisplayed(), true);
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/admin-institution/", browser.getCurrentUrl());
		
	}
	
	@Test(priority=2)
	public void dodavanjeIstrazivaca() {
		
		mainPage.getIstrazivaci().click();
		
		istrazivaciStranaPage.getDodajIstrazivacaBtn().isDisplayed();
		istrazivaciStranaPage.getDodajIstrazivacaBtn().click();
		
		istrazivaciLicniPodaciPage.getIme().isDisplayed();
		istrazivaciLicniPodaciPage.getPrezime().isDisplayed();
		istrazivaciLicniPodaciPage.getImeJednogRoditelja().isDisplayed();
		istrazivaciLicniPodaciPage.getTitulaIstrazivaca().isDisplayed();
		istrazivaciLicniPodaciPage.getRodjendan().isDisplayed();
		istrazivaciLicniPodaciPage.getDrzavaRodjenja().isDisplayed();
		istrazivaciLicniPodaciPage.getMestoRodjenja().isDisplayed();
		istrazivaciLicniPodaciPage.getOpstinaRodjenja().isDisplayed();
		istrazivaciLicniPodaciPage.getDrzavaBoravista().isDisplayed();
		istrazivaciLicniPodaciPage.getMestoBoravista().isDisplayed();
		istrazivaciLicniPodaciPage.getOpstinaBoravista().isDisplayed();
		istrazivaciLicniPodaciPage.getAdresa().isDisplayed();
		istrazivaciLicniPodaciPage.getJmbgBrojPasosa().isDisplayed();
		istrazivaciLicniPodaciPage.getEmail().isDisplayed();
		istrazivaciLicniPodaciPage.getTelefon().isDisplayed();
		istrazivaciLicniPodaciPage.getLicnaVebAdresa().isDisplayed();
		istrazivaciLicniPodaciPage.getStatusIstrazivaca().isDisplayed();
		
		istrazivaciLicniPodaciPage.setIme("Simun");
		istrazivaciLicniPodaciPage.setPrezime("Simunovic");
		istrazivaciLicniPodaciPage.setImeJednogRoditelja("Simeon");
		//TITULA MOZE "dr", "mr" ili "none"
		istrazivaciLicniPodaciPage.getTitulaIstrazivacaIzaberi("mr").click();
		istrazivaciLicniPodaciPage.setRodjendan("06.06.1980");
		istrazivaciLicniPodaciPage.setDrzavaRodjenja("Srbija");
		istrazivaciLicniPodaciPage.setMestoRodjenja("Beograd");
		istrazivaciLicniPodaciPage.setOpstinaRodjenja("Beograd");
		istrazivaciLicniPodaciPage.setDrzavaBoravista("Srbija");
		istrazivaciLicniPodaciPage.setMestoBoravista("Beograd");
		istrazivaciLicniPodaciPage.setOpstinaBoravista("Beograd");
		istrazivaciLicniPodaciPage.setAdresa("Prva ulica 1");
		istrazivaciLicniPodaciPage.setJmbgBrojPasosa("1");
		istrazivaciLicniPodaciPage.setEmail("simun@mail.com");
		istrazivaciLicniPodaciPage.setTelefon("987654321");
		istrazivaciLicniPodaciPage.setLicnaVebAdresa("simun.com");
		//STATUS MOZE BITI "Status 1", "Status 2" ili "Status 3"
		istrazivaciLicniPodaciPage.getStatusIstrazivacaIzaberi("Status 3").click();
		
		istrazivaciLicniPodaciPage.getSacuvaj().click();
		assertEquals(mainPage.getSuccess().isDisplayed(), true);
		action.moveToElement(mainPage.getSuccess()).perform();
		mainPage.getCloserBtn().click();
		
	}
	
	@Test(priority=3)
	public void angazovanjeIstrazivaca() {
		
		mainPage.getIstrazivaci().click();
		istrazivaciLicniPodaciPage.getAngazovanja().click();
		
		assertEquals(istrazivaciAngazovanjaPage.getAngazujOsobu().isDisplayed(), true);
		istrazivaciAngazovanjaPage.getAngazujOsobu().click();
		
		
		//ZVANJE MOZE BITI "Asistent", "Profesor" ili "Saradnik u nastavi"
		istrazivaciAngazovanjaPage.setZvanje("Asistent").click();
		//FUNKCIJA MOZE BITI "Funkcija 1" ili "Dekan"
		istrazivaciAngazovanjaPage.setFunkcija("Funkcija 1");
		istrazivaciAngazovanjaPage.setProcenatZaposlenosti("5");
		istrazivaciAngazovanjaPage.setOd("01.01.2016");
		
	}
	
}
