package rs.ac.uns.ftn.park;

import static org.testng.AssertJUnit.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import rs.ac.uns.ftn.park.page.LoginTestPage;
import rs.ac.uns.ftn.park.page.MainPage;

public class LoginLogout {

	private WebDriver browser;
	private LoginTestPage loginTestPage;
	private MainPage mainPage;
	
	@BeforeTest
	public void setup() {
		
		//instanciranje browsera
		browser = new FirefoxDriver();
		//implicitno vreme cekanja za pronalazenje elemenata
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//maximizuje prozor
		browser.manage().window().maximize();
		//navigacija na zeljenu stranicu
		browser.navigate().to("http://park.ftn.uns.ac.rs:8080/#/login");
		//instanciranje loginTestPage-a
		loginTestPage = new LoginTestPage(browser);
		mainPage = new MainPage(browser);
		
	}
	
	@Test
	public void uspesanLogin() throws InterruptedException {
		
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/login", browser.getCurrentUrl());
			
		loginTestPage.getUsername().isDisplayed();
		loginTestPage.getPassword().isDisplayed();
		loginTestPage.btnPrijaviSe().isDisplayed();
		
		//set username value
		loginTestPage.setUsername("miloseko@gmail.com"); //send new value
		
		//set password value
		loginTestPage.setPassword("wqt5BRBC4pXfMYQrTWge");
		
		//click signin button
		loginTestPage.btnPrijaviSe().click();
		
		
		WebElement logo = browser.findElement(By.className("logo-lg"));	
		assertEquals(logo.isDisplayed(), true);
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/admin-institution/", browser.getCurrentUrl());
		
	}
	
	@Test
	public void uspesanLogout() {
		
		assertEquals(mainPage.getPadajuciMeni().isDisplayed(), true);
		mainPage.getPadajuciMeni().click();
		assertEquals(mainPage.getOdjaviSe().isDisplayed(), true);
		mainPage.getOdjaviSe().click();
		assertEquals("http://park.ftn.uns.ac.rs:8080/#/login", browser.getCurrentUrl());
	}
	
	
}
