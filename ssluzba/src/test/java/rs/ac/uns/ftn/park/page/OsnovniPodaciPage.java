package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OsnovniPodaciPage {

	WebDriver driver;
	
	public OsnovniPodaciPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getIstrazivaciStrana() {
		return driver.findElement(By.className("fa-user"));
	}
	
	public WebElement getPodaciZaRegistar() {
		return driver.findElement(By.xpath("//*[@id='page-content']/div/div/div/div/div/div/div/div/ul/li[2]/a"));
	}
	
	public WebElement getPodaciZaProjekte() {
		return driver.findElement(By.xpath("//*[@id='page-content']/div/div/div/div/div/div/div/div/ul/li[3]/a"));
	}
	
	public WebElement getIstrazivaci() {
		return driver.findElement(By.xpath("//*[@id='page-content']/div/div/div/div/div/div/div/div/ul/li[4]/a"));
	}
	
	public WebElement getName() {
		return driver.findElement(By.name("name"));
	}
	
	public void setName(String value) {
		WebElement input = getName();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getEngName() {
		return driver.findElement(By.name("eng_name"));
	}
	
	public void setEngName(String value) {
		WebElement input = getEngName();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getState() {
		return driver.findElement(By.name("state"));
	}
	
	public WebElement getStateIzaberi(String drzava) {
		return driver.findElement(By.cssSelector("option[label='" + drzava +"']"));
	}
	
	public void setState(String value) {
		WebElement input = getState();
		input.sendKeys(value);
	}
	
	public WebElement getPlace() {
		return driver.findElement(By.name("place"));
	}
	
	public void setPlace(String value) {
		WebElement input = getPlace();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getOpstina() {
		return driver.findElement(By.name("townShipText"));
	}
	
	public void setOpstina(String value) {
		WebElement input = getOpstina();
		input.clear();
		input.sendKeys(value);
	}

	public WebElement getAddress() {
		return driver.findElement(By.name("address"));
	}
	
	public void setAddress(String value) {
		WebElement input = getAddress();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getWebAddress() {
		return driver.findElement(By.name("uri"));
	}
	
	public void setWebAddress(String value) {
		WebElement input = getWebAddress();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getEmail() {
		return driver.findElement(By.name("email"));
	}
	
	public void setEmail(String value) {
		WebElement input = getEmail();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getPhone() {
		return driver.findElement(By.name("phone"));
	}
	
	public void setPhone(String value) {
		WebElement input = getPhone();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getAcronym() {
		return driver.findElement(By.name("acro"));
	}
	
	public void setAcronym(String value) {
		WebElement input = getAcronym();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getOdustaniBtn() {
		return driver.findElement(By.cssSelector("button[ng-click='reset(Basic)']"));
	}
	
	public WebElement getSacuvajBtn() {
		return driver.findElement(By.cssSelector("button[ng-click='addctrl.saveInstitution(Basic)']"));
	}
	
}
