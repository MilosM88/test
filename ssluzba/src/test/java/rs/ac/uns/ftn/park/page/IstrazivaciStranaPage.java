package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IstrazivaciStranaPage {

	WebDriver driver;
	
	public IstrazivaciStranaPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	////*[@id='personSearch']/div[1]/div/input
	//By.cssSelector("a[href='mysite.com']");
	public WebElement getPretraziIstrazivace() {
		return driver.findElement(By.cssSelector("input[placeholder='Pretraži istraživače']"));
	}
	
	public WebElement getMigriraniPodaci() {
		return driver.findElement(By.name("migrated"));
	}
	
	public WebElement getVerifikovaniMigriraniPodaci() {
		return driver.findElement(By.name("changed"));
	}
	
	public WebElement getExcel() {
		return driver.findElement(By.className("fa-file-excel-o"));
	}
	
	// PROVERITI DA LI OVO VALJA
	public WebElement getPrezime() {
		return driver.findElement(By.cssSelector("span[ng-click='ctrl.changeOrder('lastName')']"));
	}
	
	public WebElement getDodajIstrazivacaBtn() {
		return driver.findElement(By.className("fa-plus"));
	}
}
