package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IstrazivaciLicniPodaciPage {

	WebDriver driver;
	
	public IstrazivaciLicniPodaciPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getAngazovanja() {
		return driver.findElement(By.xpath("//*[@id='page-content']/div/div/div/div/div/div/div/div/ul/li[4]/a/tab-heading"));
	}
	
	public WebElement getIme() {
		return driver.findElement(By.name("firstNameText"));
	}
	
	public void setIme(String value) {
		WebElement input = getIme();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getPrezime() {
		return driver.findElement(By.xpath("//*[@id='personSearchLastNameT']/input"));
	}
	
	public void setPrezime(String value) {
		WebElement input = getPrezime();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getImeJednogRoditelja() {
		return driver.findElement(By.name("middleName"));
	}
	
	public void setImeJednogRoditelja(String value) {
		WebElement input = getImeJednogRoditelja();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getTitulaIstrazivaca() {
		return driver.findElement(By.cssSelector("select[name='personTitle']"));
	}
	
	public WebElement getTitulaIstrazivacaIzaberi(String titula) {
		return driver.findElement(By.cssSelector("option[value='" + titula +"']"));
	}
	
	public WebElement getRodjendan() {
		return driver.findElement(By.cssSelector("input[placeholder='dd.MM.yyyy']"));
	}
	
	public void setRodjendan(String value) {
		WebElement input = getRodjendan();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getDrzavaRodjenja() {
		return driver.findElement(By.name("state"));
	}
	
	public void setDrzavaRodjenja(String value) {
		WebElement input = getDrzavaRodjenja();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getMestoRodjenja() {
		return driver.findElement(By.name("placeOfBirth"));
	}
	
	public void setMestoRodjenja(String value) {
		WebElement input = getMestoRodjenja();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getOpstinaRodjenja() {
		return driver.findElement(By.name("townShipOfBirth"));
	}
	
	public void setOpstinaRodjenja(String value) {
		WebElement input = getOpstinaRodjenja();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getDrzavaBoravista() {
		return driver.findElement(By.name("stateOfResidence"));
	}
	
	public void setDrzavaBoravista(String value) {
		WebElement input = getDrzavaBoravista();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getMestoBoravista() {
		return driver.findElement(By.name("city"));
	}
	
	public void setMestoBoravista(String value) {
		WebElement input = getMestoBoravista();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getOpstinaBoravista() {
		return driver.findElement(By.name("townShipOfResidence"));
	}
	
	public void setOpstinaBoravista(String value) {
		WebElement input = getOpstinaBoravista();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getAdresa() {
		return driver.findElement(By.name("address"));
	}
	
	public void setAdresa(String value) {
		WebElement input = getAdresa();
		input.clear();
		input.sendKeys(value);
	}
	
	// ODABERI POL
	public WebElement getPol(String pol) {
		return driver.findElement(By.cssSelector("option[label='" + pol +"']"));
	}
	
	public WebElement getJmbgBrojPasosa() {
		return driver.findElement(By.name("jmbg"));
	}
	
	public void setJmbgBrojPasosa(String value) {
		WebElement input = getJmbgBrojPasosa();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getEmail() {
		return driver.findElement(By.name("email"));
	}
	
	public void setEmail(String value) {
		WebElement input = getEmail();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getTelefon() {
		return driver.findElement(By.name("phones"));
	}
	
	public void setTelefon(String value) {
		WebElement input = getTelefon();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getLicnaVebAdresa() {
		return driver.findElement(By.name("uri"));
	}
	
	public void setLicnaVebAdresa(String value) {
		WebElement input = getLicnaVebAdresa();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getStatusIstrazivaca() {
		return driver.findElement(By.cssSelector("select[name='personTitle']"));
	}
	
	public WebElement getStatusIstrazivacaIzaberi(String status) {
		return driver.findElement(By.cssSelector("option[label='" + status +"']"));
	}

	public WebElement getOdustani() {
		return driver.findElement(By.cssSelector("button[ng-click='reset()']"));
	}

	public WebElement getSacuvaj() {
		return driver.findElement(By.cssSelector("button[ng-click='addctrl.savePerson(Basic)']"));
	}
	
}
