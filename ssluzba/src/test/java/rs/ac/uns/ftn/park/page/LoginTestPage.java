package rs.ac.uns.ftn.park.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginTestPage {

	WebDriver driver;
	
	public LoginTestPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement poljeSaLoginPorukom() {
		return driver.findElement(By.linkText("Prijavite se da počnete sa radom."));
	}
	
	public WebElement getUsername() {
		return driver.findElement(By.id("username"));
	}
	
	public void setUsername(String value) {
		WebElement input = getUsername();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement getPassword() {
		return driver.findElement(By.id("password"));
	}
	
	public void setPassword(String value) {
		WebElement input = getPassword();
		input.clear();
		input.sendKeys(value);
	}
	
	public WebElement btnOdustani() {
		return driver.findElement(By.className("btn-danger"));
	}
	
	public WebElement btnPrijaviSe() {
		return driver.findElement(By.className("btn-info"));
	}
	
}
