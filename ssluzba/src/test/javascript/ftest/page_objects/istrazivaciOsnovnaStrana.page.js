var IstrazivaciOsnovnaStrana = function() {};

IstrazivaciOsnovnaStrana.prototype = Object.create({}, {

    pretraziIstrazivace: {
        get: function() {
            return element(by.css('input[placeholder='Pretraži istraživače']'));
        }
    },

    migriraniPodaci: {
        get: function() {
            return element(by.name('migrated'));
        }
    },

    verifikovaniMigriraniPodaci: {
        get: function() {
            return element(by.name('changed'));
        }
    },

    excel: {
        get: function() {
            return element(by.className('fa-file-excel-o'));
        }
    },

    prezimeRedjanje: {
        get: function() {
            return element(by.css('span[ng-click='ctrl.changeOrder('lastName')']'));
        }
    },

    dodajIstrazivaca: {
        get: function() {
            return element(by.className('fa-plus'));
        }
    },
});

// Export klase
module.exports = IstrazivaciOsnovnaStrana;