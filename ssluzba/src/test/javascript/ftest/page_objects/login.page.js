var LoginPage = function() {}

LoginPage.prototype = Object.create({}, {

    username: {
        get: function() {
            return element(by.model('username'));
        },
        set: function(value) {
            this.username.clear();
            this.username.sendKeys(value);
        }
    },

    password: {
        get: function() {
            return element(by.model('password'));
        },
        set: function(value) {
            this.username.clear();
            this.username.sendKeys(value);
        }
    },

    prijaviSe: {
        get: function() {
            return element(by.buttonText('Prijavi se'));
        }
    },

    // Metoda za prelazak na ovu stranicu
    navigateToPage: {
        value: function() {
            browser.get('http://park.ftn.uns.ac.rs:8080/#/login');
        }
    },
    
    // Metoda za login
    login: {
        value: function(usernameString, passwordString) {
            this.username = usernameString;
            this.password = passwordString;
            this.prijaviSe.click();
        }
    }
});

// Export klase
module.exports = LoginPage;