var MenuPage = function() {};

MenuPage.prototype = Object.create({}, {

    logo: {
        get: function() {
            return element(by.className('logo-lg'));
        }
    },

    institucija: {
        get: function() {
            return element(by.className('fa-university'));
        }
    },

    istrazivaci: {
        get: function() {
            return element(by.className('fa-user'));
        }
    },

    sideBarToggle: {
        get: function() {
            return element(by.className('sidebar-toggle'));
        }
    },

    padajuciMeni: {
        get: function() {
            return element(by.css('a[class='dropdown-toggle']'));
        }
    },

    uputstvo: {
        get: function() {
            return element(by.css('a[href='app/assets/docs/Uputstvo.pdf']'));
        }
    },

    kontakt: {
        get: function() {
            return element(by.css('a[href='mailto:minis@mpn.gov.rs']'));
        }
    },

    promeniSifru: {
        get: function() {
            return element(by.css('a[ui-sref='changePassword']'));
        }
    },

    odjaviSe: {
        get: function() {
            return element(by.css('a[ng-click='logOut()']'));
        }
    },

    success: {
        get: function() {
            return element(by.css('h4[class='ui-pnotify-title']'));
        }
    },

    closer: {
        get: function() {
            return element(by.css('[class='ui-pnotify-closer']'));
        }
    }
});

// Export klase
module.exports = MenuPage;