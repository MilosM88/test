var LoginPage = require('../page_objects/login.page.js');

describe('Login page:', function() {
    var loginPage;

    beforeAll(function() {
    // Pre svega navigiramo na stranicu koju testiramo
    browser.navigate().to("http://192.168.58.25:8080/#/login");
    loginPage = new LoginPage();
  });

  it('should successfully log in', function() {
    
    //check elements visibility
    expect(loginPage.username.isPresent()).toBe(true);
    expect(loginPage.username.isDisplayed()).toBe(true);
    expect(loginPage.password.isPresent()).toBe(true);
    expect(loginPage.password.isDisplayed()).toBe(true);
    expect(loginPage.prijaviSe.isPresent()).toBe(true);
    expect(loginPage.prijaviSe.isDisplayed()).toBe(true);
		
    // //set username value
    // loginPage.username = 'miloseko@gmail.com';
    
    // //set password value
    // loginPage.password = 'wqt5BRBC4pXfMYQrTWge';
    
    loginPage.login('miloseko@gmail.com', 'wqt5BRBC4pXfMYQrTWge')
    
    //click signin button
    loginPage.prijaviSe.click();
    
    // var expectedMessage = "You are logged in as user \"admin\".";
    // //check login message
    // expect(homePage.loginConfirmationText).toEqual(expectedMessage);
  });
});
