var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var SpecReporter = require('jasmine-spec-reporter');

exports.config = {
    seleniumServerJar: '../../../node_modules/protractor/selenium/selenium-server-standalone-2.47.1.jar',
    chromeDriver: '../../../chromedriver',
    allScriptsTimeout: 20000,

    specs: [
        // 'primeri28/primer1.spec.js',
        // 'primeri28/forms/form.spec.js',
        'ftest/specs/login.js'
    ],

    capabilities: {
        'browserName': 'chrome',
    },

    directConnect: true,

    baseUrl: 'http://192.168.58.25:8080/#/login',

    framework: 'jasmine2',

    jasmineNodeOpts: {
        showColors: true,
        isVerbose: true,
        defaultTimeoutInterval: 30000,
        print: function() {}
    },

    onPrepare: function() {
        // Postavljamo prozor na fullscreen
        browser.driver.manage().window().maximize();
        
        // Implicitno cekanje 1 sekunda
        browser.manage().timeouts().implicitlyWait(1000);
        
        // Registrujemo reportere
        jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
            savePath: "./target/reports/e2e/",
            takeScreenshots: true,
            takeScreenshotsOnlyOnFailures: true,
            fixedScreenshotName: true
        }));
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: 'all',
            displaySpecDuration: true, 
            displayFailuresSummary: false, 
            displayPendingSummary: false,  
        }));
    }
};
