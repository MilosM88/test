// Konstruktor kupi vrednost elementa na koji se odnosi.
// rowElement se tada moze koristiti kao pocetak pretrage za druge elemente.
var StudentRow = function(rowElement) {
  this.rowElement = rowElement;
}

StudentRow.prototype = Object.create({}, {
  
  // Index value
  index: {
    get: function() {
      return this.rowElement.element(by.binding('studenti.indeks')).getText();
    }
  },
  
  // Ime value
  ime: {
    get: function() {
      return this.rowElement.element(by.binding('studenti.ime')).getText();
    }
  },
  
  // Prezime value
  prezime: {
    get: function() {
      return this.rowElement.element(by.binding('studenti.prezime')).getText();
    }
  },
  
  // Grad value
  grad: {
    get: function() {
      return this.rowElement.element(by.binding('studenti.grad')).getText();
    }
  },
  
  // Button View
  viewBtn: {
    get: function() {
      return this.rowElement.element(by.className('btn-info'));
    }
  },
  
  // Button Edit
  editBtn: {
    get: function() {
      return this.rowElement.element(by.className('btn-primary'));
    }
  },
  
  // Button Delete
  deleteBtn: {
    get: function() {
      return this.rowElement.element(by.className('btn-danger'));
    }
  }
});

module.exports = StudentRow;
