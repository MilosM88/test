// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var MenuPage = function() {};

// Opis polja i metoda
MenuPage.prototype = Object.create({}, {
    
    // AccountMenu dropdown
    accountMenu: {
        get: function() {
            return element(by.id('account-menu'));
        }
    },
    
    // Sign up button
    signIn: {
        get: function() {
            return element(by.xpath('//a [@ui-sref="login"]'));
        }
    },
    
    // Entities dropdown button
    entities: {
        get: function() {
            return element(by.linkText('Entities'));
        }
    },
    
    // Studenti link
    studentsLink: {
        get: function() {
            return element(by.xpath("//a[@ui-sref=\"studenti\"]"));
        }
    },
    
    // Log out button
    logOut: {
        get: function() {
            return element(by.id('logout'));
        }
    }
});

// Export klase
module.exports = MenuPage;