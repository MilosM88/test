// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var StudentCreationPage = function() {};

// Opis polja i metoda
StudentCreationPage.prototype = Object.create({}, {
    
    // Modal dialog
    modalDialog: {
        get: function() {
            return element(by.className('modal-dialog'));
        }
    },
    
    // Modal title
    modalDialogTitle: {
        get: function() {
            return element(by.id('myStudentiLabel')).getText();
        }
    },
    
    // Index input
    index: {
        get: function() {
            return element(by.name('indeks'));
        },
        set: function(value) {
            return this.index.clear().sendKeys(value);
        }
    },
    
    // Ime input
    ime: {
        get: function() {
            return element(by.name('ime'));
        },
        set: function(value) {
            return this.ime.clear().sendKeys(value);
        }
    },
    
    // Prezime input
    prezime: {
        get: function() {
            return element(by.name('prezime'));
        },
        set: function(value) {
            return this.prezime.clear().sendKeys(value);
        }
    },
    
    // Grad input
    grad: {
        get: function() {
            return element(by.name('grad'));
        },
        set: function(value) {
            return this.grad.clear().sendKeys(value);
        }
    },
    
    // Cancel button
    cancelBtn: {
        get: function() {
            return this.modalDialog.element(by.className('btn-default'));
        }
    },
    
    // Save button
    saveBtn: {
        get: function() {
            return this.modalDialog.element(by.className('btn-primary'));
        }
    },
    
    createStudent: {
        value: function(index, ime, prezime, grad) {
            this.index = index;
            this.ime = ime;
            this.prezime = prezime;
            this.grad = grad;
            this.saveBtn.click();
            
            var modalDialog = this.modalDialog
            
            // Sacekati da se ukloni modalni dialog.
            browser.wait(function() {
                return modalDialog.isPresent().then(function(isPresent) {
                    return !isPresent;
                });
            }, 5000, 'Modal dialog never disappeared');
        }
    }
});

// Export klase
module.exports = StudentCreationPage;