var StudentRow = require('./student-row.page.js');
// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var StudentsListPage = function() {};

// Opis polja i metoda
StudentsListPage.prototype = Object.create({}, {
    
    // Create button
    createBtn: {
        get: function() {
            return element(by.xpath("//button [@ui-sref=\"studenti.new\"]"));
        }
    },
    
    // Student Table
    studentsTable: {
        get: function() {
            return element(by.className('jh-table'));
        }
    },
    
    // Table rows
    tableRows: {
        get: function() {
            return element.all(by.repeater('studenti in studentis'));
        }
    },
    
    getStudentRowByIndex: {
        value: function(index) {
            var el = this.studentsTable.element(by.xpath('//*[contains(text(),"' + index + '")]/../..'));
            return new StudentRow(el);
        }
    }
});

// Export klase
module.exports = StudentsListPage;