// Prazan konstruktor
// Posto je "element" globalna funkcija, ne moramo da prosledimo nista.
var ModalDeletePage = function() {};

// Opis polja i metoda
ModalDeletePage.prototype = Object.create({}, {
    
    // AccountMenu dropdown
    modal: {
        get: function() {
            return element(by.className('modal-dialog'));
        }
    },
    
    // Confirm deletion
    confirmDelete: {
        value: function() {
            return this.modal.element(by.className('btn-danger')).click();
        }
    },
    
    // Cancel deletion
    cancelDelete: {
        value: function() {
            return this.modal.element(by.className('btn-primary')).click();
        }
    },
});

// Export klase
module.exports = ModalDeletePage;