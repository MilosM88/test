package rs.ac.uns.testdevelopment.ssluzba.pages;

import static org.testng.AssertJUnit.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ModalDeletePage {
	private WebDriver driver;
	
	public ModalDeletePage(WebDriver driver){
		this.driver = driver;
	}
	
	public WebElement getModal(){
		return driver.findElement(By.className("modal-dialog"));
	}
	public void confirmDelete() {
		getModal().findElement(By.className("btn-danger")).click();
	}
	
	public void cancelDelete(){
		getModal().findElement(By.className("btn-default")).click();
	}
}
